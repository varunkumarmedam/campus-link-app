import 'package:flutter/material.dart';

class ProfilePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height,
        width = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: Colors.white12,
        appBar: AppBar(
          backgroundColor: Colors.blue,
          elevation: 0.0,
          title: Text("My Profile"),
        ),
        body: Column(
          children: <Widget>[
            ClipPath(
              clipper: _CustomShapeClipper(),
              child: Container(
                  color: Colors.blue,
                  width: width,
                  height: height * .25,
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      padding: EdgeInsets.only(bottom: 5, right: 5, left: 5),
                      width: width * .4,
                      height: (height * .25) * .8,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                      ),
                      child: Hero(
                        tag: 'accpic',
                        child: CircleAvatar(
                          backgroundImage: NetworkImage(
                              'http://www.racemph.com/wp-content/uploads/2016/09/profile-image-placeholder.png'),
                        ),
                      ),
                    ),
                  )),
            ),
            ProfileData(),
          ],
        ));
  }
}

class _CustomShapeClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final Path path = Path();
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height * .4);
    path.lineTo(size.width * .7, size.height * .55);
    path.arcToPoint(Offset(size.width * .3, size.height * .55),
        radius: Radius.circular(size.height * .45));
    path.lineTo(0, size.height * .4);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => true;
}

class ProfileData extends StatelessWidget {
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height,
        width = MediaQuery.of(context).size.width;
    double _height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: NestedScrollView(
        physics: BouncingScrollPhysics(parent: ScrollPhysics()),
        headerSliverBuilder: (BuildContext context, bool innerBoxIsScrolled) {
          return <Widget>[
            SliverAppBar(
              expandedHeight: _height,
              floating: false,
              pinned: true,
              flexibleSpace: FlexibleSpaceBar(
                  centerTitle: true,
                  title: Text("Swipe Up to Update"),
                  background: Container(
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            colorFilter: ColorFilter.mode(
                                Color(0x50dffc), BlendMode.color),
                            fit: BoxFit.cover,
                            image: NetworkImage(
                              "https://previews.123rf.com/images/roystudio/roystudio1210/roystudio121000046/16040956-light-blue-abstract-background-cubes-pattern-in-blue-and-white-colors-may-use-to-design-book-cover.jpg",
                            ))),
                    child: Column(
                      children: <Widget>[
                        ClipPath(
                          clipper: _CustomShapeClipper(),
                          child: Container(
                              color: Color(0xff50dffc),
                              width: width,
                              height: height * .25,
                              child: Align(
                                alignment: Alignment.bottomCenter,
                                child: Container(
                                  padding: EdgeInsets.only(
                                      bottom: 5, right: 5, left: 5),
                                  width: width * .4,
                                  height: (height * .25) * .8,
                                  decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                  ),
                                  child: Hero(
                                    tag: 'accpic',
                                    child: CircleAvatar(
                                      backgroundImage: NetworkImage(
                                          'http://www.racemph.com/wp-content/uploads/2016/09/profile-image-placeholder.png'),
                                    ),
                                  ),
                                ),
                              )),
                        ),
                      ],
                    ),
                  )),
            ),
          ];
        },
        body: Center(
          child: Text("Social Media Links"),
        ),
      ),
    );
  }
}
