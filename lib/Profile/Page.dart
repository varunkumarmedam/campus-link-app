import 'package:flutter/material.dart';

class Page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
      body: Container(
          height: height,
          color: Theme.of(context).primaryColor,
          child: Stack(
            children: <Widget>[BackImage(), ProfileContent(), Barcode()],
          )),
    );
  }
}

class BackImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Positioned(
      width: width,
      top: 0,
      child: Container(
        child: Image.asset(
          'assets/profile.jpg',
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }
}

class ProfileContent extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    Color mycolor = Theme.of(context).primaryColor;
    return Center(
      child: Container(
        padding: EdgeInsets.all(30),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(30)),
          boxShadow: [
            new BoxShadow(
              color: Colors.black,
              offset: new Offset(10.0, 10.0),
              blurRadius: 20.0,
            )
          ],
        ),
        height: height / 1.7,
        width: width / 1.2,
        margin: EdgeInsets.only(
          top: height / 4,
        ),
        child: Column(children: <Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.all(0.0),
              child: Text(
                "Mary Linda",
                style: TextStyle(fontSize: 25, color: mycolor),
              ),
            ),
          ),
          Divider(),
          Row(
            children: <Widget>[
              Icon(
                Icons.edit,
                color: mycolor,
                size: 30,
              ),
              Container(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    "U17CS416",
                    style: TextStyle(color: mycolor, fontSize: 25),
                  ))
            ],
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.developer_board,
                color: mycolor,
                size: 30,
              ),
              Flexible(
                  child: Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "Computer Science",
                        style: TextStyle(color: mycolor, fontSize: 25),
                        overflow: TextOverflow.fade,
                      )))
            ],
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.mail,
                color: mycolor,
                size: 30,
              ),
              Flexible(
                  child: Container(
                      padding: EdgeInsets.all(10),
                      child: Text(
                        "hackingraja2018@gmail.com",
                        style: TextStyle(color: mycolor, fontSize: 25),
                        overflow: TextOverflow.fade,
                      )))
            ],
          ),
        ]),
      ),
    );
  }
}

class Barcode extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Container(
      margin: EdgeInsets.only(top: height / 1.2),
      child: Image.asset(
        'assets/barcode.png',
        width: width,
        height: height / 7,
      ),
    );
  }
}
