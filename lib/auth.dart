import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';

abstract class BaseAuth {
  Future<String> signinwithemailandpassword(String mail, String password);
  Future<String> createuseraaccount(
      String mail, String password, String displayname);
  Future<FirebaseUser> currentuserstate();
  Future<void> onsignout();
  Future<void> sendpasswordresetmail(String mail);
}

class Auth implements BaseAuth {
  Future<String> signinwithemailandpassword(
      String mail, String password) async {
    FirebaseUser auth = await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: mail.toLowerCase(), password: password);
    return auth.uid;
  }

  Future<String> createuseraaccount(
      String mail, String password, String displayname) async {
    FirebaseUser user = await FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: mail, password: password);
    UserUpdateInfo info = UserUpdateInfo();
    var photourl =
        'http://www.racemph.com/wp-content/uploads/2016/09/profile-image-placeholder.png';
    info.displayName = displayname;
    info.photoUrl = photourl;
    user.updateProfile(info);
    await Firestore.instance.collection('users').document(user.uid).setData(
        {'username': displayname, 'photourl': photourl, 'email': user.email});
    return user.uid;
  }

  Future<FirebaseUser> currentuserstate() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    return user;
  }

  Future<void> onsignout() async {
    FirebaseAuth.instance.signOut();
  }

  Future<void> sendpasswordresetmail(String mail) {
    return FirebaseAuth.instance.sendPasswordResetEmail(email: mail);
  }
}
