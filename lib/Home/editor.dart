import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:zefyr/zefyr.dart';
import 'dart:convert';
class MyImageDelegate implements ZefyrImageDelegate<ImageSource>{
  @override
  Widget buildImage(BuildContext context, String imageSource) {
  debugPrint(imageSource);
  final image= NetworkImage(imageSource);
  return Image(image: image,height: 300,);
  }

  @override
  Future<String> pickImage(ImageSource source) async {
  
  final file = await ImagePicker.pickImage(source: source);
  debugPrint("ca;;ed");
  if(file==null) return null;
  else{
    debugPrint("called");
    StorageReference firebasestorage= FirebaseStorage.instance.ref().child('child2.png');
    StorageUploadTask task= firebasestorage.putFile(file);
   var value = await task.onComplete;
         StorageTaskSnapshot uploadTask;
   if(value.error==null){
      uploadTask=value;
   }
   debugPrint(uploadTask.ref.getDownloadURL().toString());
   return uploadTask.ref.getDownloadURL().then((url){
     return url;
   });
  }
  }
}

class EditorPage extends StatefulWidget {

  @override
  _EditorPageState createState() => _EditorPageState();
}

class _EditorPageState extends State<EditorPage> {

@override
Widget build(BuildContext context) {
  return DefaultTabController(
    length: 2,
      child: Scaffold(
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(title: Text('Example Form'),
      bottom: TabBar(
        tabs: <Widget>[
          Tab(
            text: "Write Ur Story",
            ),
            Tab(
              text: "Edit ur story",
            )
        ],
      ),),
      body: TabBarView(
        children: <Widget>[
          Write(),
          Edit()
        ],
      )
    ),
  );
}
}

class Write extends StatefulWidget {

  @override
  _WriteState createState() => _WriteState();
}

class _WriteState extends State<Write> {
ZefyrController _zefyrController;

FocusNode _focusNode;

@override
  void initState() {
    final notusDocument= NotusDocument.fromJson([{"insert":"Ststjsfb fsj\n"}]);
    _focusNode=FocusNode();
    _zefyrController= ZefyrController(notusDocument);
    super.initState();
  }

  @override
  void dispose() {
    _zefyrController.document.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
final editor = ZefyrField(
  imageDelegate: MyImageDelegate(),
    height: 200.0,
    decoration: InputDecoration(labelText: 'Description',
    hintMaxLines: 10
    ),
    controller: _zefyrController,
    focusNode: _focusNode,
    autofocus: false,
    physics: ClampingScrollPhysics(),
  );
  final form = ListView(
    children: <Widget>[
      TextField(decoration: InputDecoration(labelText: 'Article Heading')),
      TextField(decoration: InputDecoration(labelText: 'Topic Name')),
      editor,
      FlatButton(
        child: Text("submit"),
        onPressed: (){
        String s=jsonEncode(_zefyrController.document.toJson());
          debugPrint(s);
          },
      ),
    ],
  );

    return ZefyrScaffold(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: form,
        ),
      );
  }
}

class Edit extends StatefulWidget {

  @override
  _EditState createState() => _EditState();
}

class _EditState extends State<Edit> {
ZefyrController _zefyrController;
FocusNode _focusNode;
@override
  void initState() {
    final notusDocument= NotusDocument.fromJson([{"insert":"Ststjsfb fsj nfwyekyekyek\n"},{"insert":"​","attributes":{"embed":{"type":"image","source":"https://firebasestorage.googleapis.com/v0/b/awesomeproject-b20f9.appspot.com/o/child1.png?alt=media&token=aabf2f5d-f351-4d54-8170-385a89f4a2c9"}}},{"insert":"\nGmyekye yekyekyeky gskyeludlduldngzng kydlud\n\n"}]);
    _focusNode=FocusNode();
    _zefyrController= ZefyrController(notusDocument);
    super.initState();
  }
  @override
  void dispose() {
    _zefyrController.document.close();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {

final editor = ZefyrField(
    height: 200.0, // set the editor's height
    decoration: InputDecoration(labelText: 'Description',
    hintMaxLines: 10
    ),
    controller: _zefyrController,
    focusNode: _focusNode,
    autofocus: false,
    physics: ClampingScrollPhysics(),
  );
  final form = ListView(
    children: <Widget>[
      TextField(decoration: InputDecoration(labelText: 'Article Heading')),
      TextField(decoration: InputDecoration(labelText: 'Topic Name')),
      editor,
      FlatButton(
        child: Text("submit"),
        onPressed: (){
          jsonEncode(_zefyrController.document.toJson());
          },
      ),
     
    ],
  );


    return ZefyrScaffold(
          child: ZefyrView(
            document: NotusDocument.fromJson([{"insert":"Ststjsfb fsj nfwyekyekyek\n"},{"insert":"​","attributes":{"embed":{"type":"image","source":"https://firebasestorage.googleapis.com/v0/b/awesomeproject-b20f9.appspot.com/o/child1.png?alt=media&token=aabf2f5d-f351-4d54-8170-385a89f4a2c9"}}},{"insert":"\nGmyekye yekyekyeky gskyeludlduldngzng kydlud\n\n"}]),
            imageDelegate: MyImageDelegate(),
          )
    );
  }
}