import 'package:flutter/material.dart';

class Reading extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
          body: NestedScrollView(
      headerSliverBuilder: (context,bool isScrolled){
      return <Widget>[
        SliverAppBar(
          title: Text('IOI'),
          centerTitle: true,
          forceElevated: isScrolled,
          floating: true,
          snap: true,
        )
      ];
      },
      body: Container(
        color: Colors.white,
        child: ListView(
        physics: ClampingScrollPhysics(),
        children: <Widget>[
          Text("Some Title over Here",overflow: TextOverflow.fade,style: TextStyle(
            fontSize: 35,
            fontWeight: FontWeight.w500
          ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Chip(
                avatar: CircleAvatar(backgroundImage: NetworkImage("http://www.racemph.com/wp-content/uploads/2016/09/profile-image-placeholder.png")),
                label: Text('Saikumar'),
                backgroundColor: Color(0xFFE5E7E9),
              ),
              Text('12/12/2019',overflow: TextOverflow.fade,
              style: TextStyle(
                color: Colors.grey
              ),
              ),
            ],
          ),
          Container(
            height: 200,
            width: double.infinity,
            child: Image.network('https://www.seoclerk.com/pics/303521-1E6WAl1418390942.jpg',fit:BoxFit.cover),
          ),
          Container(
            margin: EdgeInsets.only(top: 15,left: 8,right: 8),
            child: Text("""At a luxury resort in Baja California Sur, everyone was recovering from a long day sharing how hard it felt to be getting older.

Some of the participants walked pensively along the Pacific Coast at sunset. Others read from the resort bookshelf, choosing from sections labeled: “What can death teach me about life?” and “What are the unexpected pleasures of aging?”

The next day, the group would place stickers with ageist slurs all over their chests, arms and faces, and then hurl the stickers into a fire. Later, there would be healing sessions focused on intergenerational collaboration and accepting mortality.

That some of those on retreat were in their 30s and 40s did not strike them as odd.

The resort, called Modern Elder Academy and located in El Pescadero, Mexico, opened in November. Guests don’t check in and out as at a traditional retreat; they submit “applications” for one-week programs and, if accepted, pay “tuition” of 5,000 to secure a room and three locally sourced meals a day.

Modern Elder was started by a hotelier turned Silicon Valley entrepreneur, and it is aimed at workers in the digital economy — those who feel like software is speeding up while they are slowing down, no matter how old they really are. Tech is a place where investors are wary of funding any entrepreneur born before Operation Desert Storm, where Intel is under investigation by the Equal Employment Opportunity Commission for age discrimination, where giants like Google and IBM regularly face the specter of class-action lawsuits from workers north of 40.

In and around San Francisco, the conventional wisdom is that tech jobs require a limber, associative mind and an appetite for risk — both of which lessen with age. As Silicon Valley work culture becomes American work culture, these attitudes are spreading to all industries. More workers are finding themselves in the curious position of presenting as old while still being — technically, actuarially — quite young. And Modern Elder sees a business opportunity in selling them coping workshops, salt-air yoga and access to a shaman.""",maxLines: 80,overflow: TextOverflow.fade,
style: TextStyle(
  fontSize: 16
),
),
          )
        ],
      ),
      )
      ),
    );
  }
}