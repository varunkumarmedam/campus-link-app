// import 'package:flare_flutter/flare_actor.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_pagewise/flutter_pagewise.dart';
import './reading.dart';

class Articles extends StatefulWidget {
  @override
  _ArticlesState createState() => _ArticlesState();
}

class _ArticlesState extends State<Articles>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;

    return RefreshIndicator(
      displacement: 30,
      backgroundColor: Colors.black,
      color: Colors.white,
      onRefresh: _refresh,
      child: PagewiseListView(
        primary: true,
        showRetry: true,
        // loadingBuilder: (context) => Container(
        //     height: 100,
        //     width: 100,
        //     child: FlareActor(
        //       "assets/loader.flr",
        //       alignment: Alignment.center,
        //       animation: "Loading",
        //       fit: BoxFit.fill,
        //     )),
        
        pageSize: 3,
        physics: BouncingScrollPhysics(),
        itemBuilder: (context, snapshot, index) {
          return Column(
            children: <Widget>[
              GestureDetector(
                onTap: () {
                  Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) => Reading()));
                },
                child: Card(
                  margin: EdgeInsets.only(bottom: 15, left: 18, right: 18),
                  child: Container(
                      color: Colors.white,
                      padding: EdgeInsets.all(10.0),
                      height: height / 4,
                      width: width - 15,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Container(
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceAround,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text(
                                    snapshot['domainname'],
                                    style: TextStyle(color: Colors.grey),
                                  ),
                                  Text(
                                    snapshot['title'],
                                    maxLines: 2,
                                    overflow: TextOverflow.fade,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 19,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Text(
                                    snapshot['hinttext'],
                                    style: TextStyle(
                                        color: Colors.grey, fontSize: 12),
                                  ),
                                  Text(
                                    snapshot['postedby'],
                                    style: TextStyle(color: Colors.black),
                                  )
                                ],
                              ),
                            ),
                          ),
                          Container(
                            height: (height / 7),
                            width: height / 7,
                            color: Colors.pinkAccent,
                            child: CachedNetworkImage(
                              imageUrl: 'https://nofile.io/f/pQDyJYETy01/download.jpeg',
                              fit: BoxFit.cover,
                              fadeInDuration: Duration(seconds: 1),
                              placeholder: (context, str) => Container(
                                    color: Color(0xFFF3F3F3),
                                  ),
                            ),
                          )
                        ],
                      )),
                ),
              ),
            ],
          );
        },
        pageFuture: (snapshot) async {
          await Future.delayed(Duration(seconds: 2));
          QuerySnapshot qs = await Firestore.instance
              .collection("articles")
              .orderBy('title')
              .limit(3)
              .getDocuments();
          debugPrint('Articles are loaded');
          return qs.documents;
        },
      ),
    );
  }

  @override
  bool get wantKeepAlive => true;

  Future<void> _refresh() {
    return Future.delayed(Duration(seconds: 1));
  }
}

class SlideRightRoute extends PageRouteBuilder {
  final Widget widget;
  SlideRightRoute({this.widget})
      : super(pageBuilder: (BuildContext context, Animation<double> animation,
            Animation<double> secondaryAnimation) {
          return widget;
        }, transitionsBuilder: (BuildContext context,
            Animation<double> animation,
            Animation<double> secondaryAnimation,
            Widget child) {
          return new SlideTransition(
            position: new Tween<Offset>(
              begin: const Offset(1.0, 0.0),
              end: Offset.zero,
            ).animate(animation),
            child: child,
          );
        });
}
