import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:share/share.dart';
import 'Home.dart';

class Messages extends StatelessWidget {
  myicon(String ic) {
    switch (ic) {
      case 'computer':
        return Icon(
          Icons.computer,
          size: 35,
        );
        break;
      default:
        return Icon(
          Icons.sentiment_very_satisfied,
          size: 35,
        );
    }
  }
  final HomeTheme data =HomeTheme();
  @override
  Widget build(BuildContext context) {
    return Container(
        child: StreamBuilder(
            stream: Firestore.instance
                .collection('notifications')
                .orderBy("time", descending: true)
                .snapshots(),
            builder: (context, snapshot) {
              switch (snapshot.connectionState) {
                case ConnectionState.waiting:
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                  break;
                case ConnectionState.none:
                  return Center(
                    child: Text("Check Your Connection!"),
                  );
                  break;
                case ConnectionState.done:
                  return Center(child: Text("Loading"));
                  break;
                case ConnectionState.active:
                  return ListView.builder(
                      physics: BouncingScrollPhysics(),
                      itemCount: snapshot.data.documents.length,
                      itemBuilder: (context, index) {
                        DocumentSnapshot ds1 = snapshot.data.documents[index];
                        // var date = DateTime.now().toString().substring(8, 10);
                        // var temp_date = ds1['time'].toString().substring(8, 10);
                        return Column(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10)),
                                    gradient: LinearGradient(
                                      colors: [
                                        data.color1,
                                        data.color2
                                      ]
                                    )
                              ),
                              margin: EdgeInsets.all(10),
                              padding: EdgeInsets.all(10),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      "25-02-2019",
                                      style: TextStyle(
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                  Slidable(
                                    delegate: new SlidableDrawerDelegate(),
                                    actionExtentRatio: 0.25,
                                    child: Container(
                                        margin:
                                            EdgeInsets.fromLTRB(20, 0, 0, 0),
                                        decoration: BoxDecoration(
                                            color: Colors.white,
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(10))),
                                        child: ExpansionTile(
                                          title: Row(
                                            children: <Widget>[
                                              Container(
                                                  height: 40,
                                                  padding: EdgeInsets.fromLTRB(
                                                      20, 4, 20, 6),
                                                  decoration: BoxDecoration(
                                                      color: Colors.red,
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  5))),
                                                  transform:
                                                      Matrix4.translationValues(
                                                          -30.0, 0, 0.0),
                                                  child:
                                                      myicon(ds1['category'])),
                                              Container(
                                                child: Text(
                                                  ds1['head'],
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                  style: TextStyle(
                                                      fontFamily: 'Comfortaa',
                                                      fontSize: 20),
                                                ),
                                              ),
                                            ],
                                          ),
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(8.0),
                                              child: Text(ds1["content"]),
                                            ),
                                          ],
                                        )),
                                    secondaryActions: <Widget>[
                                      Container(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(5))),
                                        child: IconSlideAction(
                                            caption: 'Share',
                                            color: Colors.indigo,
                                            icon: Icons.share,
                                            onTap: () {
                                              Share.share(
                                                  "*BIHER Utility App Notification* : " +
                                                      ds1["content"]);
                                            }),
                                      )
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      });
              }
            }));
  }
}
