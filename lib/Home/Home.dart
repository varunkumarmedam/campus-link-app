import 'package:flutter/material.dart';
import 'tab2.dart';
import 'tab1.dart';
import '../mydrawer.dart';
import 'package:infinityx/auth.dart';
import 'package:intro_views_flutter/intro_views_flutter.dart';
import 'package:intro_views_flutter/Models/page_view_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class HomePage extends StatefulWidget {
  HomePage({this.signout2, this.auth});
  final BaseAuth auth;
  final VoidCallback signout2;

  @override
  HomePageState createState() {
    return new HomePageState();
  }
}

enum Show { show, dshow }

class HomeModel extends Model {
  String fabindex;
  changefab() {
    debugPrint("called");
    fabindex = "0";
    notifyListeners();
  }
}

class HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  Show show = Show.show;
  final HomeModel homemodel = HomeModel();

  final pages = [
    PageViewModel(
        pageColor: const Color(0xFF03A9F4),
        bubbleBackgroundColor: Colors.white,
        body: Text('We are trying to globalise the servicves to students',
            style: TextStyle(fontSize: 18)),
        title: Text(
          'Services',
          style: TextStyle(fontSize: 23),
        ),
        textStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
        mainImage: Image.asset(
          'assets/services.png',
          height: 285.0,
          width: 285.0,
          alignment: Alignment.center,
        )),
    PageViewModel(
      pageColor: const Color(0xFF8BC34A),
      bubbleBackgroundColor: Colors.white,
      body: Text(
          'Get Your Attendance Live and Chat with the people with same intrest Explore the ease of life',
          style: TextStyle(fontSize: 18)),
      title: Text('Attendance', style: TextStyle(fontSize: 23)),
      mainImage: Image.asset(
        'assets/attendence.png',
        height: 285.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
    ),
    PageViewModel(
      pageColor: const Color(0xFF607D8B),
      bubbleBackgroundColor: Colors.white,
      body: Text(
          'We trying to build larger community of students in india by includeing all the students under the roof IOI',
          style: TextStyle(fontSize: 18)),
      title: Text('Community', style: TextStyle(fontSize: 23)),
      mainImage: Image.asset(
        'assets/community.png',
        height: 400.0,
        width: 285.0,
        alignment: Alignment.center,
      ),
      textStyle: TextStyle(fontFamily: 'MyFont', color: Colors.white),
    ),
  ];

  @override
  void initState() {
    final tabcontroller =
        TabController(vsync: this, length: 2, initialIndex: 0);
    tabcontroller.addListener(() {
      if (tabcontroller.index == 0) {
        homemodel.changefab();
      }
    });
    super.initState();
  }

  HomeTheme data = HomeTheme();
  @override
  Widget build(BuildContext context) {
    return ScopedModel<HomeModel>(
        model: homemodel,
        child: (show == Show.dshow)
            ? DefaultTabController(
                length: 2,
                child: Scaffold(
                  appBar: GradientAppBar(
                    backgroundColorStart: data.color1,
                    backgroundColorEnd: data.color2,
                    centerTitle: true,
                    title: Text(
                      "Utility App",
                      style: TextStyle(fontSize: 27),
                    ),
                    bottom: TabBar(
                      indicatorColor: Colors.white,
                      tabs: <Widget>[
                        Tab(
                          text: "Articles",
                        ),
                        Tab(
                          text: "Messages",
                        ),
                      ],
                    ),
                  ),
                  endDrawer: MyDrawer(
                    signout2: widget.signout2,
                    auth: widget.auth,
                  ),
                  body: TabBarView(
                    physics: BouncingScrollPhysics(),
                    children: <Widget>[
                      Articles(),
                      Messages(),
                    ],
                  ),
                ))
            : Scaffold(
                body: Builder(
                  builder: (context) => IntroViewsFlutter(
                        pages,
                        onTapDoneButton: () {
                          setState(() {
                            show = Show.dshow;
                          });
                        },
                        pageButtonTextStyles: TextStyle(
                          color: Colors.white,
                          fontSize: 14.0,
                        ),
                      ), //IntroViewsFlutter
                ),
              ));
  }
}

class HomeTheme {
  Color color1 = Color.fromRGBO(214, 74, 119, 1);
  Color color2 = Color.fromRGBO(242, 146, 171, 1);
}
