import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';

class EventsPage extends StatefulWidget {
  @override
  EventsPageState createState() {
    return new EventsPageState();
  }
}

class EventsPageState extends State<EventsPage> {
  @override
  Widget build(BuildContext context) {
    double sheight = MediaQuery.of(context).size.height;
    double swidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        title: Text('EVENTS'),
        backgroundColor: Theme.of(context).primaryColor,
        centerTitle: true,
        elevation: 50,
      ),
      body: FutureBuilder(
        future: Firestore.instance.collection("events").getDocuments(),
        builder: (context, snapshot) {
          QuerySnapshot qs = snapshot.data;
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Container(
                child: Center(child: CircularProgressIndicator()),
              );
              break;
            default:
              return LiquidPullToRefresh(
                color: Theme.of(context).primaryColor,
                showChildOpacityTransition: false,
                springAnimationDurationInMilliseconds: 400,
                onRefresh: (() => testRefesh()),
                child: ListView.builder(
                  reverse: false,
                  physics: BouncingScrollPhysics(),
                  itemCount: qs.documents.length,
                  itemBuilder: (context, i) {
                    DocumentSnapshot ds = qs.documents[i];
                    return Card(
                        margin: EdgeInsets.only(left: 10, right: 10, top: 15),
                        elevation: 50,
                        child: Container(
                          alignment: Alignment.bottomCenter,
                          height: sheight / 2.5,
                          width: swidth - 20,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: CachedNetworkImageProvider(ds['url']),
                                  fit: BoxFit.cover)),
                          child: Container(
                            alignment: Alignment.bottomCenter,
                            height: sheight / 2.5,
                            width: swidth - 20,
                            decoration: BoxDecoration(
                                image: DecorationImage(
                                    image:
                                        CachedNetworkImageProvider(ds['url']),
                                    fit: BoxFit.cover)),
                            child: Container(
                                width: swidth,
                                height: (sheight / 3.5) / 3,
                                decoration: BoxDecoration(
                                    color: Colors.transparent.withOpacity(0.6)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Text(
                                      ds['title'],
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500,
                                          fontSize: 25),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Text(
                                          'PostedBy : ${ds['by']}',
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        Text(
                                            ds['timestamp']
                                                .toString()
                                                .substring(0, 10),
                                            style:
                                                TextStyle(color: Colors.white))
                                      ],
                                    )
                                  ],
                                )),
                          ),
                        ));
                  },
                ),
              );
          }
        },
      ),
    );
  }

  Future<void> testRefesh() {
    return Future.delayed(Duration(seconds: 1));
  }
}
