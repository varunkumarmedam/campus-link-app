import 'package:flutter/material.dart';
import './auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:convert';

class LoginPage extends StatelessWidget {
  final VoidCallback signedin;
  LoginPage({this.signedin});
  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: Colors.pink[500],
      body: ListView(
        children: <Widget>[
          Column(children: <Widget>[
            Container(
              height: height / 2 + 10,
              child: Image.asset(
                "assets/sslaucher.png",
                fit: BoxFit.cover,
              ),
            ),
            Container(
              color: Colors.white,
              height: 4,
              width: double.infinity,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 50, bottom: 10),
              child: RaisedButton(
                elevation: 0.0,
                padding:
                    EdgeInsets.only(left: 60, right: 60, top: 13, bottom: 13),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0)),
                color: Colors.pink,
                child: Text(
                  "SIGNIN HERE",
                  style: TextStyle(fontSize: 26, color: Colors.white),
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute<Null>(
                          builder: (BuildContext context) {
                            return AuthScreen(
                              state: "Login",
                              auth: Auth(),
                              signedin: signedin,
                            );
                          },
                          fullscreenDialog: true));
                },
              ),
            ),
            RaisedButton(
              elevation: 0.0,
              padding:
                  EdgeInsets.only(left: 60, right: 60, top: 13, bottom: 13),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0)),
              color: Colors.white,
              child: Text(
                "SINGNUP",
                style: TextStyle(fontSize: 26, color: Colors.pink[500]),
              ),
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute<Null>(
                        builder: (BuildContext context) {
                          return AuthScreen(
                            state: "Register",
                            auth: Auth(),
                            signedin: signedin,
                          );
                        },
                        fullscreenDialog: true));
              },
            )
          ]),
        ],
      ),
    );
  }
}

class AuthScreen extends StatefulWidget {
  AuthScreen({this.state, this.auth, this.signedin});
  final VoidCallback signedin;
  final BaseAuth auth;
  final String state;
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  var emailedit = TextEditingController();
  var passedit = TextEditingController();
  var pinedit = TextEditingController();
  var useredit = TextEditingController();
  var rollnoedit = TextEditingController();
  var forpass = TextEditingController();
  var formkey = GlobalKey<FormState>();
  var _scaffoldkey = GlobalKey<ScaffoldState>();
  List collg = <String>["BHARATH UNIVERSITY", "I year", "II year"];
  List branch = <String>["CSE", "ECE", "AERO", "AEROSPACE", "MECH"];
  List sections = <String>["Select The Branch First"];
  String selectedcollg;
  String selectbranch;
  String selectsection;
  double hei = 60.0;
  double wid = 400;

  @override
  Widget build(BuildContext context) {
    Widget email = TextFormField(
      keyboardType: TextInputType.emailAddress,
      controller: emailedit,
      decoration: InputDecoration(
        labelText: "Email",
        hintText: "Enter Ur Mail",
      ),
      validator: ((value) {
        if (value.length < 11 && !value.contains("@")) {
          return "Invalid Mail";
        }
      }),
    );

    Widget password = TextFormField(
      controller: passedit,
      obscureText: true,
      decoration: InputDecoration(
        labelText: "Password",
        hintText: "atleast 8 characters",
      ),
      validator: ((value) {
        if (value.length < 8) {
          return "Password must be atleast 8 charaters";
        }
      }),
    );

    Widget user = TextFormField(
      controller: useredit,
      decoration: InputDecoration(
        labelText: "Username",
        hintText: "AS in collg Id card",
      ),
      validator: ((value) {
        if (value.length < 8) {
          return "username must be atleast 8 character long";
        }
      }),
    );

    Widget confirmpass = TextFormField(
      decoration: InputDecoration(
        labelText: "Confirm Password",
      ),
      obscureText: true,
      validator: ((value) {
        if (value != passedit.text) {
          return "Password didn't match";
        }
      }),
    );

    Widget forpass = Row(mainAxisAlignment: MainAxisAlignment.end, children: [
      FlatButton(
        color: Colors.white,
        child: Text(
          "Forgot Password ??",
          textAlign: TextAlign.right,
          style: TextStyle(color: Colors.pink),
        ),
        onPressed: forgotpass,
      )
    ]);

    Widget pin = TextFormField(
      controller: pinedit,
      keyboardType: TextInputType.text,
      decoration:
          InputDecoration(labelText: "pin", hintText: "get the pin from staff"),
      validator: ((value) {
        if (value.length < 10) {
          return "Invalid Pin";
        }
        if (base64.encode(utf8.encode(rollnoedit.text)).toUpperCase() !=
            value) {
          return "CrossCheck the Pin";
        }
      }),
    );

    Widget button = AnimatedContainer(
        color: Colors.pink[500],
        duration: Duration(milliseconds: 300),
        height: hei,
        width: wid,
        child: Padding(
            padding: EdgeInsets.only(top: 0),
            child: RaisedButton(
              padding:
                  EdgeInsets.only(left: 70, right: 70, top: 18, bottom: 18),
              color: Colors.pink[500],
              splashColor: Colors.white,
              child: Text(
                widget.state,
                style: TextStyle(color: Colors.white, fontSize: 22),
              ),
              onPressed: buttonpressed,
            )));

    Widget collgDD = Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Flexible(
            child: DropdownButtonFormField(
              value: selectedcollg,
              items: collg.map((value) {
                return new DropdownMenuItem<String>(
                  child: Text(
                    value,
                    style: TextStyle(fontSize: 13),
                  ),
                  value: value,
                );
              }).toList(),
              hint: Text("select ur collg"),
              onChanged: (value) {
                setState(() {
                  selectedcollg = value;
                });
              },
              validator: ((value) {
                if (value == null) {
                  return "this field is required";
                }
              }),
            ),
          ),
          Flexible(
            child: DropdownButtonFormField(
              value: selectbranch,
              items: branch.map((value) {
                return new DropdownMenuItem<String>(
                  child: Text(
                    value,
                    style: TextStyle(fontSize: 13),
                  ),
                  value: value,
                );
              }).toList(),
              hint: Text("Branch"),
              onChanged: (value) {
                setState(() {
                  selectbranch = value;
                });
                update();
              },
              validator: ((value) {
                if (value == null) {
                  return "this field is required";
                }
              }),
            ),
          )
        ]);

    Widget section = DropdownButtonFormField(
      value: selectsection,
      hint: Text("SECTION"),
      items: sections.map((value) {
        return new DropdownMenuItem<String>(
          child: Text(value),
          value: value,
        );
      }).toList(),
      onChanged: (value) {
        setState(() {
          selectsection = value;
        });
      },
      validator: ((value) {
        if (value == "Select The Branch First" || value == null) {
          return "section is required";
        }
      }),
    );

    Widget rollno = TextFormField(
      controller: rollnoedit,
      keyboardType: TextInputType.text,
      decoration:
          InputDecoration(labelText: "rollno", hintText: "University RollNo"),
      validator: ((value) {
        if (value.length < 8) {
          return "Enter correct RollNo";
        }
        if (base64.encode(utf8.encode("value")).toUpperCase() != pinedit.text) {
          print(base64.encode(utf8.encode("value")));
          return "Invalid Rollno or Pin";
        }
      }),
    );

    Widget loginwidgets() {
      return Padding(
        padding: const EdgeInsets.only(top: 20, left: 15, right: 15),
        child: Column(
          children: <Widget>[email, password, forpass, button],
        ),
      );
    }

    Widget registerwidgets() {
      return Padding(
        padding: const EdgeInsets.only(top: 20, left: 10, right: 10),
        child: Column(
          children: <Widget>[
            pin,
            collgDD,
            section,
            rollno,
            user,
            email,
            password,
            confirmpass,
            button
          ],
        ),
      );
    }

    return Scaffold(
      key: _scaffoldkey,
      backgroundColor: Colors.white,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.pink[500]),
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: Text(
          widget.state,
          style: TextStyle(
              color: Colors.pink[500],
              fontSize: 20,
              fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
      ),
      body: ListView(physics: BouncingScrollPhysics(), children: <Widget>[
        Form(
            key: formkey,
            child:
                widget.state == "Login" ? loginwidgets() : registerwidgets()),
      ]),
    );
  }

  buttonpressed() async {
    if (validate()) {
      try {
        if (widget.state == "Login") {
          await widget.auth.signinwithemailandpassword(emailedit.text, passedit.text);
        } else {
          await widget.auth
              .createuseraaccount(emailedit.text, passedit.text, username());
        }
        Navigator.of(context).pop();
        widget.signedin();
      } catch (e) {
        _scaffoldkey.currentState.showSnackBar(SnackBar(
          content: Text("something went wrong try again"),
        ));
      }
    }
  }

  bool validate() {
    if (formkey.currentState.validate()) {
      return true;
    } else {
      return false;
    }
  }

  update() async {
    final firestore = Firestore();

    DocumentSnapshot snapshot = await firestore
        .collection("sections")
        .document(selectbranch.toLowerCase())
        .get();
    setState(() {
      sections.clear();
      for (var key in snapshot.data.keys) {
        sections.add(snapshot[key]);
      }
    });
  }

  String username() {
    var username = useredit.text + selectedcollg.substring(0, 1);
    username = username + rollnoedit.text.substring(0, 2);
    username = username + selectsection;
    return username;
  }

  forgotpass() {
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (context) {
          forpass.clear();
          return AlertDialog(
            title: Text("Reset your Password", textAlign: TextAlign.center),
            content: TextField(
              controller: forpass,
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  labelText: "MAIL",
                  hintText: "Enter Ur registered Mail",
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(12),
                      borderSide: BorderSide(
                        style: BorderStyle.solid,
                      ))),
            ),
            actions: <Widget>[
              Row(
                children: <Widget>[
                  FlatButton(child: Text("SEND"), onPressed: resetmail),
                  FlatButton(
                    child: Text("Back"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              )
            ],
          );
        });
  }

  resetmail() async {
    try {
      await widget.auth.sendpasswordresetmail(forpass.text.toLowerCase());
      Navigator.of(context).pop();
      _scaffoldkey.currentState.showSnackBar(SnackBar(
        duration: Duration(seconds: 5),
        content: Text("YaY!! Password reset Instructions has sent to ur mail"),
      ));
    } catch (e) {
      Navigator.of(context).pop();
      _scaffoldkey.currentState.showSnackBar(SnackBar(
        duration: Duration(seconds: 2),
        content: Text("Email Doest Registered WithUs"),
      ));
    }
  }
}
