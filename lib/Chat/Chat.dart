import 'dart:async';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:connectivity/connectivity.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:infinityx/models/chat_model/chat1_model.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class Chat extends StatefulWidget {
  final String uid;
  Chat({this.uid});
  @override
  ChatState createState() {
    return new ChatState();
  }
}

class ChatState extends State<Chat> {
  ChatTheme data = ChatTheme();
  final controller = TextEditingController();
  final scrollcontroller = ScrollController();
  final ConnectModel connectmodel = ConnectModel();
  var connect = Connectivity();
  StreamSubscription<ConnectivityResult> _streamSubscription1;
  @override
  void initState() {
    _streamSubscription1 =
        connect.onConnectivityChanged.listen((ConnectivityResult result) {
      debugPrint('poraa1');
      (result == ConnectivityResult.none)
          ? connectmodel.change()
          : connectmodel.data();
      debugPrint('poraa3');
    });
    super.initState();
  }

  @override
  void dispose() {
    _streamSubscription1.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ScopedModel<ConnectModel>(
      model: connectmodel,
      child: Scaffold(
        appBar: GradientAppBar(
          backgroundColorStart: data.color1,
          backgroundColorEnd: data.color2,
          title: Text("Chat Room"),
          actions: <Widget>[
            Padding(
                padding: const EdgeInsets.all(10.0),
                child: IconButton(
                    icon: Icon(Icons.help_outline),
                    onPressed: () {
                      showDialog(
                          barrierDismissible: true,
                          context: context,
                          builder: (context) => Dialog(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15.0)),
                                child: Container(
                                  height: 400,
                                  child: Column(
                                    children: <Widget>[
                                      Text("Tips",
                                          style: TextStyle(
                                              fontSize: 20,
                                              color: Colors.green)),
                                      Divider()
                                    ],
                                  ),
                                ),
                              ));
                    }))
          ],
        ),
        body: SafeArea(
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Expanded(child: buildListview()),
                  Container(
                    child: TextField(
                      controller: controller,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(
                              borderSide: BorderSide(color: Colors.black),
                              borderRadius: BorderRadius.circular(10.0)),
                          hintText: "ur message here",
                          suffixIcon: ScopedModelDescendant<ConnectModel>(
                            builder: (context, _, connectmodel) {
                              return (connectmodel.connect1 !=
                                      "ConnectivityResult.none")
                                  ? IconButton(
                                      icon: Icon(
                                        Icons.send,
                                        color: data.color1,
                                      ),
                                      onPressed: () {
                                        sendmessege();
                                        scrollcontroller.animateTo(0.0,
                                            curve: Curves.bounceOut,
                                            duration:
                                                Duration(milliseconds: 400));
                                      })
                                  : SizedBox(
                                      height: 1,
                                      width: 1,
                                    );
                            },
                          )),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildListview() {
    var cf = Firestore.instance
        .collection("chats")
        .document("17csea")
        .collection("17csea")
        .reference();
    return StreamBuilder<QuerySnapshot>(
      stream: cf
          .orderBy('timestamp_client', descending: true)
          .limit(20)
          .snapshots(),
      builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.waiting:
            return Center(
              child: CircularProgressIndicator(),
            );
            break;
          default:
            if (snapshot.hasError) {
              return Center(
                child: Text("went on error"),
              );
            } else {
              return ListView.builder(
                  physics: BouncingScrollPhysics(),
                  controller: scrollcontroller,
                  shrinkWrap: true,
                  reverse: true,
                  itemCount: snapshot.data.documents.length,
                  itemBuilder: (context, index) =>
                      buildmesseges(index, snapshot.data.documents[index]));
            }
        }
      },
    );
  }

  Widget buildmesseges(int index, DocumentSnapshot ds) {
    return (ds['senderid'] == widget.uid)
        ? Align(
            alignment: Alignment.centerRight,
            child: Container(
                margin: EdgeInsets.only(
                    left: (MediaQuery.of(context).size.width) * 0.25,
                    right: 10,
                    top: 8),
                padding: EdgeInsets.all(10),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      bottomLeft: Radius.circular(10),
                      topRight: Radius.circular(15),
                    ),
                    color: data.color1),
                child: RichText(
                    textAlign: TextAlign.end,                    
                    softWrap: true,
                    text: TextSpan(
                      children: [
                        TextSpan(
                            text: ds['content'],
                            style: TextStyle(fontSize: 18)),
                        TextSpan(
                          text: '  4:30',
                        )
                      ],
                    ))))
        : Align(
            alignment: Alignment.centerLeft,
            child: Card(
              margin: EdgeInsets.only(
                  right: MediaQuery.of(context).size.width * 0.25,
                  bottom: 10,
                  left: 10),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadiusDirectional.circular(15)),
              child: Container(
                padding: EdgeInsets.all(10),
                child: Text(
                  ds['content'],
                  textAlign: TextAlign.start,
                  overflow: TextOverflow.fade,
                ),
              ),
            ));
  }

  void sendmessege() {
    if (controller.text.isNotEmpty) {
      DocumentReference df = Firestore.instance
          .collection('chats')
          .document('17csea')
          .collection('17csea')
          .document(DateTime.now().millisecondsSinceEpoch.toString());
      df.setData({
        'content': controller.text.trim(),
        'senderid': widget.uid,
        'timestamp_client': DateTime.now().millisecondsSinceEpoch.toString(),
        'timestamp_server': FieldValue.serverTimestamp()
      });
      controller.clear();
    } else {
      Fluttertoast.showToast(
          msg: "Write Something..!",
          backgroundColor: Colors.black38,
          textColor: Colors.white,
          timeInSecForIos: 1,
          gravity: ToastGravity.BOTTOM);
    }
  }
}

class ChatTheme {
  Color color1 = Color(0xff9d80ff);
  Color color2 = Color(0xffb9bbeb);
}
