import 'dart:async';
import 'package:flutter/material.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'ClassRoom.dart';

class Atten extends StatefulWidget {
  @override
  AttenState createState() {
    return new AttenState();
  }
}

class AttenState extends State<Atten> with SingleTickerProviderStateMixin {
  Animation animation;
  AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(
        duration: Duration(milliseconds: 4000), vsync: this);
    animation = IntTween(begin: 100, end: 70).animate(CurvedAnimation(
        parent: animationController, curve: Curves.fastOutSlowIn));
    animationController.forward();
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ClassTheme data =ClassTheme();
    return RefreshIndicator(
      color: Theme.of(context).primaryColor,
      onRefresh: onrefresh,
      child: ListView(
        shrinkWrap: true,
        padding: EdgeInsets.only(),
        children: <Widget>[
          Card(
            elevation: 50,
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    data.color1,
                    data.color2
                  ]
                )
              ),
              width: MediaQuery.of(context).size.width * .9,
              height: 60,
              child: Center(
                child: Text(
                  'EVEN SEMISTER ~2018-19',
                  textScaleFactor: 1.5,
                ),
              ),
            ),
          ),
          SizedBox(
            height: 40,
          ),
          CircularPercentIndicator(
            startAngle: 0.0,
            percent: 0.7,
            progressColor: Colors.indigo,
            lineWidth: 20,
            radius: MediaQuery.of(context).size.width * .8,
            center: AnimatedBuilder(
              animation: animationController,
              builder: (context, child) {
                return Text(animation.value.toString(),
                    style:
                        TextStyle(fontSize: 100, fontStyle: FontStyle.normal));
              },
            ),
            circularStrokeCap: CircularStrokeCap.round,
            animation: true,
            animateFromLastPercent: true,
            animationDuration: 800,
          ),
        ],
      ),
    );
  }

  Future<void> onrefresh() {
    return Future.delayed(Duration(seconds: 3));
  }
}
