import 'package:flutter/material.dart';
import 'package:infinityx/models/classroom_models/class_model.dart';
import './atten.dart';
import './Timetable.dart';
import './resources.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class ClassRoom extends StatelessWidget {

final ResourcesModel resourcesModel =ResourcesModel();

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: GradientAppBar(
          backgroundColorStart: Color(0xfff57e9b),
          backgroundColorEnd: Color(0xffefc280),
          title: Text("MY ClassRoom"),
          centerTitle: true,
          bottom: TabBar(
            tabs: <Widget>[
      Tab(
        icon: Icon(Icons.person),
        text: "Attendance",
      ),
      Tab(
        icon: Icon(Icons.tab),
        text: "TimeTable",
      ),
      Tab(
        icon: Icon(Icons.library_books),
        text: "Resources",
      )
            ],
          ),
        ),
        body: TabBarView(
            children: <Widget>[Atten(), TimeTable(), Resources()],
          ),
        ),
    );
  }
}
