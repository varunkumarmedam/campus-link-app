import 'package:flutter/material.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:infinityx/models/classroom_models/class_model.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import '../taost.dart';

class Resources extends StatefulWidget {
  @override
  _ResourcesState createState() => _ResourcesState();
}

ResourcesModel resourcesModel = ResourcesModel();

class _ResourcesState extends State<Resources> {
  @override
  void initState() {
    checkerp();
    super.initState();
  }

  void showbottomsheet(context) async {
    double height = MediaQuery.of(context).size.height;
    await showModalBottomSheet<void>(
        context: context,
        builder: (context) {
          return Container(
              height: height / 2,
              child: StateBuilder(
                  blocs: [resourcesModel],
                  stateID: 'list1',
                  builder: (_) => (resourcesModel.sublist.isEmpty)
                      ? Center(child: CircularProgressIndicator())
                      : ListView.separated(
                          itemCount: resourcesModel.sublist.length,
                          separatorBuilder: (context, _) {
                            return Divider(
                              color: Theme.of(context).primaryColor,
                            );
                          },
                          itemBuilder: (context, index) {
                            return ListTile(
                              title: Text(resourcesModel.sublist[index]),
                              onTap: () {
                                resourcesModel.change(index);
                                Navigator.of(context).pop();
                                resourcesModel.fetchsublist(resourcesModel.sublist[index]);
                              },
                            );
                          },
                        )));
        });
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: refresh,
      child: Container(
          height: 50,
          width: 50,
          color: Color(0xFFF3F3F3),
          child: Column(
            children: <Widget>[
              RawMaterialButton(
                shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.orangeAccent)),
                child: StateBuilder(
                  blocs: [resourcesModel],
                  stateID: "button1",
                  builder: (_) => Text(resourcesModel.selectsubject),
                ),
                onPressed: () {
                  showbottomsheet(context);
                  if (resourcesModel.sublist.isEmpty) {
                    resourcesModel.fetchsubjects();
                  }
                },
              ),
              StateBuilder(
                  blocs: [resourcesModel],
                  stateID: "listView",
                  builder: (_) => (resourcesModel.selectsubject ==
                          'Select the subject')
                      ? SizedBox()
                      : (resourcesModel.sub
                                  .containsKey(resourcesModel.selectsubject))
                          ?  ListView(
                            shrinkWrap: true,
                            physics: BouncingScrollPhysics(),
                              children: buildlist(),
                            )
                          :Center(
                              child: CircularProgressIndicator(),
                            ))
            ],
          )),
    );
  }

  Future<void> refresh() {
    return Future.delayed(Duration(seconds: 2));
  }

  void checkerp() async {
    if (await PermissionHandler()
            .checkPermissionStatus(PermissionGroup.storage) !=
        PermissionStatus.granted) {
      await PermissionHandler().requestPermissions([
        PermissionGroup.storage,
        PermissionGroup.sms,
        PermissionGroup.locationAlways
      ]).then((map) {
        if (map[PermissionGroup.storage] == PermissionStatus.denied ||
            map[PermissionGroup.storage] == PermissionStatus.restricted) {
          Navigator.of(context).pop();
          Fluttertoast.showToast(
            msg: "permissions Denied please enable Required permissions",
            toastLength: Toast.LENGTH_SHORT,
          );
        }
      });
    }
  }

List<Widget> buildlist() {
  List<Widget> l=[];
  debugPrint('${resourcesModel.sub[resourcesModel.selectsubject].toString()}');
  resourcesModel.sub[resourcesModel.selectsubject].forEach((k,v){
    l.add(
    ListTile(title: Text(k),
    trailing: InkWell(
      child: Icon(Icons.file_download,color: Colors.teal,),
      onTap: ()=>
      download(v,k),
          ),)
        );
        });
        return l;
      }
      
        download(v,k) async{
          
        getExternalStorageDirectory().then((dir){
          if(FileSystemEntity.typeSync('${dir.path}/IOI/resources/${resourcesModel.selectsubject}')==FileSystemEntityType.notFound){
            Directory('${dir.path}/IOI/resources/${resourcesModel.selectsubject}').create(recursive: true);
          }else if(FileSystemEntity.typeSync('${dir.path}/IOI/resources/${resourcesModel.selectsubject}/$k')==FileSystemEntityType.notFound){
            Toast1().showToast(msg: "File already exists");
            return;
          }
         FlutterDownloader.enqueue(
         url: v,
         savedDir: '${dir.path}/IOI/resources/${resourcesModel.selectsubject}',
         fileName: k,
         openFileFromNotification: true,
         showNotification: true
       );
        });
        }
}  
