import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sticky_headers/sticky_headers.dart';
import 'ClassRoom.dart';

class TimeTable extends StatelessWidget {
  getWeek() {
    var week = DateTime.now().weekday.toInt();
    if (week > 5) {
      return 0;
    } else {
      return week - 1;
    }
  }

  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: getWeek(),
      length: 5,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Scaffold(
          backgroundColor: Color(0xFFF3F3F3),
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(400),
            child: TabBar(
              labelColor: Colors.black,
              indicatorColor: Color(0xfff57e9b),
              isScrollable: true,
              tabs: [
                Tab(text: "MONDAY"),
                Tab(text: "TUESDAY"),
                Tab(text: "WEDNESDAY"),
                Tab(text: "THURSDAY"),
                Tab(text: "FRIDAY"),
              ],
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              TableData(0),
              TableData(0),
              Text("happy Wednesday"),
              Text("Thuss Thursday"),
              Text("Hurray Its Friday")
            ],
          ),
        ),
      ),
    );
  }
}

class TableData extends StatelessWidget {
  final ClassTheme data = new ClassTheme();
  final int myweek;
  TableData(this.myweek);
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: Firestore.instance
            .collection("timetable")
            .document('17csea')
            .collection('17csea')
            .document(myweek.toString())
            .get(),
        builder: (context, snapshot) {
          DocumentSnapshot len = snapshot.data;
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(
                child: CircularProgressIndicator(),
              );
              break;
            case ConnectionState.none:
              return Center(
                child: Text("Check Your Connection!"),
              );
              break;
            case ConnectionState.active:
              return Center(child: Text("Loading"));
              break;
            case ConnectionState.done:
              return ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: len.data.length,
                itemBuilder: (context, index) {
                  Map mymap = snapshot.data[index.toString()];
                  return Card(
                      margin: EdgeInsets.all(10),
                      elevation: 5,
                      child: StickyHeader(
                        header: Container(
                            alignment: Alignment.center,
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(
                              gradient: LinearGradient(
                                colors: [
                                  data.color1,
                                  data.color2
                                ]
                              )
                            ),
                            child: Text(
                              mymap['subject'],
                              style: TextStyle(fontSize: 25,color: Colors.white),
                            ),
                            ),
                        content: Padding(
                          padding: const EdgeInsets.all(20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Text(mymap['code'],
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.black38,
                                      )),
                                  Container(
                                      padding: EdgeInsets.all(15),
                                      decoration: BoxDecoration(),
                                      child: Text(mymap['staff'],
                                          style: TextStyle(fontSize: 20))),
                                  Text(mymap['time'],
                                      style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.black38,
                                      ))
                                ],
                              ),
                              Container(
                                alignment: Alignment.topRight,
                                child: CircleAvatar(
                                  radius: 35,
                                  backgroundImage: NetworkImage(
                                      'http://www.racemph.com/wp-content/uploads/2016/09/profile-image-placeholder.png'),
                                ),
                              )
                            ],
                          ),
                        ),
                      ));
                },
              );
          }
        });
  }
}
