import 'package:flutter/material.dart';
import './auth.dart';
import './RootPage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
    primaryColor: Color.fromARGB(255,0,0,102),
    brightness: Brightness.light,
    fontFamily: 'Comfortaa',
    primaryTextTheme: TextTheme(
      title: TextStyle(color: Colors.white)
    ),
    primaryIconTheme: IconThemeData(
      color: Colors.white
    ),
    iconTheme: IconThemeData(
      color: Colors.white
    )
        ),
        title: "Infintyx",
        home: RootPage(auth: Auth(),)
        );

  }
}
