import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:infinityx/Home/editor.dart';
import './Events/Events.dart';
import './Chat/Chat.dart';
import './Profile/ProfilePage.dart';
import './Profile/Page.dart';
import './ClassRoom/ClassRoom.dart';
import './Home/tab1.dart';
import './Gallery/Gallery.dart';
import './auth.dart';
import 'package:firebase_auth/firebase_auth.dart';
import './Admin/attendance.dart';
import 'Admin/AdminPage.dart';

class MyDrawer extends StatefulWidget {
  MyDrawer({this.signout2, this.auth});
  final BaseAuth auth;
  final VoidCallback signout2;
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  final _scaffoldkey = GlobalKey<ScaffoldState>();
  void signout() async {
    try {
      await widget.auth.onsignout();
      widget.signout2();
    } catch (e) {
      showsnackbar("error has occured try again");
    }
  }

  void showsnackbar(String msg) {
    _scaffoldkey.currentState.showSnackBar(
      SnackBar(content: Text(msg)),
    );
  }

  String useremail = "Loading...";
  String url1 =
      "http://www.racemph.com/wp-content/uploads/2016/09/profile-image-placeholder.png";
  String username = "Loading...";
  FirebaseUser m;
  @override
  void initState() {
    super.initState();
    widget.auth.currentuserstate().then((value) {
      setState(() {
        if (value.photoUrl != null) {
          url1 = value.photoUrl;
        }
        if (value.displayName != null) {
          username = value.displayName;
        }
        if (value.email != null) {
          useremail = value.email;
        }
        m = value;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 260,
      child: Drawer(
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: <Widget>[
            UserAccountsDrawerHeader(
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: CachedNetworkImageProvider(
                        'https://cdn-images-1.medium.com/max/1200/1*zH86wd-3URNeM_MoeaBQuQ.jpeg'),
                    fit: BoxFit.cover),
              ),
              currentAccountPicture: InkWell(
                child: Hero(
                  tag: 'accpic',
                  child: CircleAvatar(
                      backgroundImage: CachedNetworkImageProvider(url1)),
                ),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ProfileData()));
                },
              ),
              accountEmail: Text(useremail),
              accountName: Text(username),
            ),
            ListTile(
              title: Text("My Class Room"),
              leading: Icon(
                Icons.group,
                color: Color(0xfff57e9b),
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, SlideRightRoute(widget: ClassRoom()));
              },
            ),
            ListTile(
              title: Text("Chat Room"),
              leading: Icon(
                Icons.chat,
                color: Color(0xff9d80ff),
              ),
              onTap: () {
                Navigator.pop(context);
                Auth().currentuserstate().then((auth) {
                  if (auth.uid != null) {
                    Navigator.push(
                        context, SlideRightRoute(widget: Chat(uid: auth.uid)));
                  } else {
                    showsnackbar("Login da dey");
                  }
                });
              },
            ),
            ListTile(
                title: Text("Events"),
                leading: Icon(Icons.event),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                      context, SlideRightRoute(widget: EventsPage()));
                }),
            ListTile(
              title: Text("Gallery"),
              leading: Icon(
                Icons.photo_library,
                color: Color(0xff1fc2b1),
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, SlideRightRoute(widget: Gallery()));
              },
            ),
            ListTile(
              title: Text("Results"),
              leading: Icon(
                Icons.mail,
                color: Colors.indigo,
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, SlideRightRoute(widget: Attendance()));
              },
            ),
            ListTile(
              title: Text("Payments"),
              leading: Icon(
                Icons.payment,
                color: Colors.pink,
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(context, SlideRightRoute(widget: AdminPage()));
              },
            ),
                ListTile(
              title: Text("Student Clubs"),
              leading: Icon(
                Icons.group,
                color: Colors.greenAccent,
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context).push(MaterialPageRoute(builder: (context)=>EditorPage(),fullscreenDialog: true));
              },
            ),
            ListTile(
              title: Text("Account Settings"),
              leading: Icon(
                Icons.account_circle,
                color: Colors.blue,
              ),
              onTap: () {
                Navigator.pop(context);
                Navigator.of(context)
                    .push(MaterialPageRoute(builder: (context) => Page()));
              },
            ),
            Divider(),
            ListTile(
              title: Text("Sign out"),
              onTap: () {
                signout();
              },
            )
          ],
        ),
      ),
    );
  }
}
