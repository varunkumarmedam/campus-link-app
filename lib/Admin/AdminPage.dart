import 'package:flutter/material.dart';
import 'Timetable.dart';

class AdminPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Staff'),
          bottom: TabBar(
            indicatorWeight: 3,
            indicatorColor: Colors.white,
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.table_chart),
                text: 'Time Table',
              ),
              Tab(
                icon: Icon(Icons.place),
                text: "My Space",
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: <Widget>[
            TimeTable(),
            Container(),
          ],
        ),
      ),
    );
  }
}
