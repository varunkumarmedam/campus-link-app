import 'package:flutter/material.dart';
import 'package:infinityx/models/admin_models/atten_model.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:flare_flutter/flare_actor.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';

class Attendance extends StatefulWidget {
  @override
  _AttendanceState createState() => _AttendanceState();
}

class _AttendanceState extends State<Attendance> {
  final AttendanceModel attendanceModel = AttendanceModel();
  @override
  void initState() {
    attendanceModel.fetchitems();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: refresh,
      child: ScopedModel<AttendanceModel>(
        model: attendanceModel,
        child: Scaffold(
                      appBar: GradientAppBar(
                        backgroundColorStart: Color(0xff519751),
                        backgroundColorEnd: Color(0xff8ae089),
                        title: Text("Attendance"),
                        centerTitle: true,
                        actions: <Widget>[
                          Container(
                            alignment: Alignment.center,
                            margin: EdgeInsets.only(right: 10),
                            child: ScopedModelDescendant<AttendanceModel>(
                              rebuildOnChange: (attendanceModel.list == null &&
                                      attendanceModel.qs == null)
                                  ? false
                                  : true,
                              builder: (context, _, model) {
                                return Text(
                                  "${model.l}/${model.list.length}",
                                  style:
                                      TextStyle(fontSize: 19, fontWeight: FontWeight.w700),
                                );
                              },
                            ),
                          )
                        ],
                      ),
                      body: ScopedModelDescendant<AttendanceModel>(
                        rebuildOnChange: (attendanceModel.list.isEmpty) ? true : false,
                        builder: (context, _, model) {
                          debugPrint('rebuit');
                          return (model.qs == null && model.list.isEmpty)
                              ? Center(
                                  child: Container(
                                    height: 90,
                                    width: 90,
                                    child: FlareActor(
                                      "assets/loader.flr",
                                      animation: "Loading",
                                      alignment: Alignment.center,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                )
                              : GetAttendance(attendanceModel);
                        },
                      ),
                    ),
      ),
    );
            }
          
            Future<void> refresh() {
              return Future.delayed(Duration(seconds: 3));
  }
}

class GetAttendance extends StatelessWidget {
  final AttendanceModel attendanceModel;
  GetAttendance(this.attendanceModel);

  @override
  Widget build(BuildContext context) {
    return ScopedModelDescendant<AttendanceModel>(
      builder: (context, _, model) {
        return ListView.separated(
          physics: BouncingScrollPhysics(),
          itemCount: model.qs.documents.length + 1,
          separatorBuilder: (context, index) {
            return SizedBox(
              height: 3,
            );
          },
          itemBuilder: (context, index) {
            return (index != model.qs.documents.length)
                ? Card(
                    elevation: 100,
                    margin: EdgeInsets.only(left: 4, right: 4),
                    color: (model.list[index] == false)
                        ? Colors.white
                        : Colors.lightBlueAccent,
                    child: ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.grey,
                      ),
                      title: Text(model.qs.documents[index]['rollno']),
                      subtitle: Text(model.qs.documents[index]['name']),
                      trailing: Text(
                          model.qs.documents[index]['attendance'].toString()),
                      onTap: () => attendanceModel.change(index),
                    ),
                  )
                : Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      RaisedButton(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20)),
                        onPressed: () => debugPrint("Got it macha :)"),
                      )
                    ],
                  );
          },
        );
      },
    );
  }
}
