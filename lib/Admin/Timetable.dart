import 'dart:core';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sticky_headers/sticky_headers.dart';

class TimeTable extends StatelessWidget {
  getWeek() {
    var week = DateTime.now().weekday.toInt();
    if (week > 5) {
      return 0;
    } else {
      return week - 1;
    }
  }

  Widget build(BuildContext context) {
    return DefaultTabController(
      initialIndex: getWeek(),
      length: 5,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(400),
            child: TabBar(
              labelColor: Colors.black,
              indicatorColor: Theme.of(context).primaryColor,
              isScrollable: true,
              tabs: [
                Tab(text: "MONDAY"),
                Tab(text: "TUESDAY"),
                Tab(text: "WEDNESDAY"),
                Tab(text: "THURSDAY"),
                Tab(text: "FRIDAY"),
              ],
            ),
          ),
          body: TabBarView(
            children: <Widget>[
              TableData(1),
              TableData(1),
              TableData(1),
              TableData(1),
              Text("Hurray Its Friday")
            ],
          ),
        ),
      ),
    );
  }
}

class TableData extends StatelessWidget {
  int _myweek;
  TableData(this._myweek);

  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return FutureBuilder(
        future: Firestore.instance
            .collection("timetable")
            .document('17csea')
            .collection('17csea')
            .document(_myweek.toString())
            .get(),
        builder: (context, snapshot) {
          DocumentSnapshot len = snapshot.data;
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(
                child: CircularProgressIndicator(),
              );
              break;
            case ConnectionState.none:
              return Center(
                child: Text("Check Your Connection!"),
              );
              break;
            case ConnectionState.active:
              return Center(child: Text("Loading"));
              break;
            case ConnectionState.done:
              return ListView.builder(
                physics: BouncingScrollPhysics(),
                itemCount: len.data.length,
                itemBuilder: (context, index) {
                  Map mymap = snapshot.data[index.toString()];
                  return InkWell(
                    splashColor: Colors.white30,
                    child: Card(
                        margin: EdgeInsets.all(10),
                        elevation: 5,
                        child: StickyHeader(
                          header: Container(
                              alignment: Alignment.center,
                              padding: EdgeInsets.all(15),
                              color: Theme.of(context).primaryColor,
                              child: Text(
                                mymap['section'],
                                style: TextStyle(
                                    fontSize: 25, color: Colors.white),
                              )),
                          content: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Container(
                                    width: width / 3,
                                    padding: EdgeInsets.all(10),
                                    child: Text(mymap['time'],
                                        style: TextStyle(fontSize: 22)),
                                  ),
                                  Flexible(
                                    child: Container(
                                      child: Text("Subject Operating system",
                                          overflow: TextOverflow.fade,
                                          style: TextStyle(fontSize: 25)),
                                    ),
                                  ),
                                ],
                              ),
                              
                            ],
                          ),
                        )),
                  );
                },
              );
          }
        });
  }
}
