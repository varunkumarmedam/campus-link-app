import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:infinityx/taost.dart';
class ResourcesModel extends StatesRebuilder{
  String selectsubject = 'Select the subject';
  List<String> sublist = [];
  Firestore firestore=Firestore.instance;
  var sub=Map<String,Map<String,String>>();
  change(int index) {
    selectsubject = sublist[index];
  rebuildStates(ids: ['button1','listView']);
  }

  fetchsubjects() {
    Firestore.instance
        .collection("resources")
        .document("17csea")
        .get()
        .then((DocumentSnapshot ds) {
      for (var item in ds['subjects']) {
        sublist.add(item);
      }
      rebuildStates(ids: ['list1']);
    });
  }
  fetchsublist(String subject){
    
      Firestore.instance.collection("resources").document('17csea').collection(subject).document(subject).get().then((ds){
      if(ds.exists){
      Map map =Map<String,String>.from(ds['reslist']);
      sub.putIfAbsent(subject, ()=>map);
      rebuildStates(ids: ['listView']);
      }else{
     Toast1().showToast(msg: 'NO Documents Found');
      }
    });
  }
}