import 'package:scoped_model/scoped_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class AttendanceModel extends Model {
  QuerySnapshot qs;
  List<bool> list = [];
  int l = 0;
  fetchitems() async {
    Firestore firestore=Firestore.instance;
    firestore.settings(persistenceEnabled: false);    
    qs = await firestore
        .collection("allstu")
        .document('17csea')
        .collection('17csea')
        .getDocuments();
    if(qs.documents.isNotEmpty){
    list = List.generate(qs.documents.length, (index) => false);
    notifyListeners();
    }
  }

  change(int index) {
    bool li = list[index];
    list[index] = !li;
    (li) ? l-- : l++;
    notifyListeners();
  }
}