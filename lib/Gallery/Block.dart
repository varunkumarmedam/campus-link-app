import 'package:flutter/material.dart';
import 'package:transformer_page_view/transformer_page_view.dart';

class ImageBlock extends StatefulWidget {
  ImageBlock({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ImageBlockState createState() =>  _ImageBlockState();
}

List<String> images = ["assets/assasin.jpg", "assets/emma.png", "assets/captain.png"];
List<String> text0 = ["Image1","Image2","Image3"];
List<String> text1 = ["Image1_data","Image1_data","Image1_data"];

class ImageTest extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return  TransformerPageView(
        loop: true,
        viewportFraction: 0.8,
        transformer:  PageTransformerBuilder(
            builder: (Widget child, TransformInfo info) {
          return  Padding(
            padding:  EdgeInsets.all(10.0),
            child:  Material(
              elevation: 4.0,
              textStyle:  TextStyle(color: Colors.white),
              borderRadius:  BorderRadius.circular(10.0),
              child:  Stack(
                fit: StackFit.expand,
                children: <Widget>[
                   ParallaxImage.asset(
                    images[info.index],
                    position: info.position,
                  ),
                  
                   Positioned(
                    child:  Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                         ParallaxContainer(
                          child:  Text(
                            text0[info.index],
                            style:  TextStyle(fontSize: 15.0),
                          ),
                          position: info.position,
                          translationFactor: 300.0,
                        ),
                         SizedBox(
                          height: 8.0,
                        ),
                         ParallaxContainer(
                          child:  Text(text1[info.index],
                              style:  TextStyle(fontSize: 18.0)),
                          position: info.position,
                          translationFactor: 200.0,
                        ),
                      ],
                    ),
                    left: 10.0,
                    right: 10.0,
                    bottom: 10.0,
                  )
                ],
              ),
            ),
          );
        }),
        itemCount: 3);
  }
}

class _ImageBlockState extends State<ImageBlock> {
  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      body:  Padding(
          padding:  EdgeInsets.fromLTRB(10.0, 40.0, 10.0, 30.0),
          child:  ImageTest()),
    );
  }
}