import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:gradient_app_bar/gradient_app_bar.dart';
import 'Block.dart';

class Gallery extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // Future<String> url = showImage();
    // url.then();
    // print(url);
    return Scaffold(
        backgroundColor: Colors.grey,
        body: CustomScrollView(
          slivers: <Widget>[
            SliverGradientAppBar(
              backgroundColorStart: Color(0xff1fc2b1),
              backgroundColorEnd: Color(0xff00d386),
              title: Text("gallery"),
            ),
            SliverList(
              delegate: SliverChildBuilderDelegate((context, index) {
                return (index == 0)
                    ? SizedBox(
                        height: 210,
                        child: ListView(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          children: <Widget>[
                            GestureDetector(
                              child: Container(
                                  margin: EdgeInsets.all(10),
                                  color: Colors.pink,
                                  height: 200,
                                  width: 200,
                                  child: Image.network(
                                      'https://firebasestorage.googleapis.com/v0/b/awesomeproject-b20f9.appspot.com/o/2.png?alt=media&token=f723e247-a44f-4b76-903c-a2dea5f1e9e2')),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ImageBlock()));
                              },
                            ),
                            Container(
                              margin: EdgeInsets.all(10),
                              color: Colors.pink,
                              height: 200,
                              width: 200,
                            ),
                            Container(
                              margin: EdgeInsets.all(10),
                              color: Colors.pink,
                              height: 200,
                              width: 200,
                            ),
                            Container(
                              margin: EdgeInsets.all(10),
                              color: Colors.pink,
                              height: 200,
                              width: 200,
                            ),
                          ],
                        ))
                    : Container(
                        margin: EdgeInsets.all(10),
                        height: 100,
                        color: Colors.black,
                      );
              }),
            )
          ],
        ));
  }
}

futureWidget() async {
  StorageReference ref = FirebaseStorage.instance.ref().child('2.png');
  String url = await ref.getDownloadURL();
  return Container(
      margin: EdgeInsets.all(10),
      color: Colors.pink,
      height: 200,
      width: 200,
      child: Image.network(url));
}
