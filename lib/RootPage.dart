import 'package:flutter/material.dart';
import 'LoginPage.dart';
import './auth.dart';
import 'Home/Home.dart';

enum Authstatus { signedin, notsignedin }

class RootPage extends StatefulWidget {
  RootPage({this.auth});
  final BaseAuth auth;

  @override
  RootPageState createState() {
    return new RootPageState();
  }
}

class RootPageState extends State<RootPage> {
  Authstatus _authstatus = Authstatus.notsignedin;

  @override
  void initState() {
    super.initState();
    widget.auth.currentuserstate().then((userid) {
      setState(() {
        _authstatus =
            (userid != null) ? Authstatus.signedin : Authstatus.notsignedin;
      });
    });
  }

  void _signedin() {
    setState(() {
      _authstatus = Authstatus.signedin;
    });
  }

  void _signout1() {
    setState(() {
      _authstatus = Authstatus.notsignedin;
    });
  }

  @override
  Widget build(BuildContext context) {
    return _authstatus == Authstatus.notsignedin
        ? LoginPage(
            signedin: _signedin,
          )
        : HomePage(signout2: _signout1, auth: widget.auth);
  }
}
